from bs4 import BeautifulSoup
from datetime import date, datetime, timedelta
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import requests
import json
import time
import re


today = date.today()
d1 = today.strftime("%Y/%m/%d")
checkIn = d1.replace("/","-") #current day
checkOut = datetime.strptime(today.strftime("%Y/%m/%d"), "%Y/%m/%d").date() + timedelta(days=1) #current day + 1, data type = date time, need convert to string

URL = "https://www.booking.com/hotel/vn/phuong-anh-ho-chi-minh-city.en-gb.html?" \
      "label=gen173nr-1DCAEoggI46AdIM1gEaPQBiAEBmAEJuAEHyAEM2AED6AEBiAIBqAIDuALk8M39BcACAdICJDdkMGM5ZjRiLTNkMjctNGEzNy1iMTgwLTFjZDQwMGFiY2Q1MdgCBOACAQ;" \
      "sid=68fb5d7ea4d291cc10e5bea2139f6c61;" \
      "all_sr_blocks=57187701_266112761_0_0_0;" \
      "checkin=" + checkIn + ";" \
      "checkout=" + str(checkOut) + ";" \
      "dest_id=-3730078;" \
      "dest_type=city;" \
      "dist=0;" \
      "group_adults=2;" \
      "group_children=0;" \
      "hapos=3;" \
      "highlighted_blocks=57187701_266112761_0_0_0;" \
      "hpos=3;" \
      "no_rooms=1;" \
      "room1=A,A;" \
      "sb_price_type=total;" \
      "sr_order=popularity;" \
      "sr_pri_blocks=57187701_266112761_0_0_0__51200000;" \
      "srepoch=1605597294;srpvid=02bd32f67d21014a;" \
      "type=total;" \
      "ucfs=1&#hotelTmpl"
    
print(URL)
headers = {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9'}
# option selenium
options = Options()
# options.add_argument('--headless')
# options.add_argument('--disable-gpu')  # Last I checked this was necessary.
# options.add_argument('start-maximized')
# options.add_argument('disable-infobars')
driver = webdriver.Chrome("./chromedriver", chrome_options=options)

driver.get(URL)
soup = BeautifulSoup(driver.page_source, 'lxml')
print(URL)
def get_content():
    text = soup.select('.hp-description')[0].get_text()
    text = text.replace("\n", " ")
    return text #string

string = []
def get_table():
    PAGE = requests.get(URL, headers=headers)
    soup1 = BeautifulSoup(PAGE.content, 'lxml')
    htmltable = soup1.find('table')
    trs = htmltable.find_all('tr')
    
    try:
        print("try")
        for i in range(1, len(trs)):
            title = trs[i].find("span", {'class': 'hprt-roomtype-icon-link'}).get_text()
            col1 = trs[i].find("td", {'class': 'hprt-table-cell -first hprt-table-cell-roomtype droom_seperator'}).get_text()
            col2 = trs[i].find("span", {'class': 'bui-u-sr-only'}).get_text()
            col3 = trs[i].find("div", {'class': 'bui-price-display__value prco-inline-block-maker-helper prco-font16-helper'}).get_text()
            title = title.replace("\n", " ")
            col1 = col1.replace("\n", " ")
            col2 = col2.replace("\n", "")
            col3 = col3.replace("\n", "")
            col3 = col3.replace("\xa0", " ")
            array = re.findall(r'[0-9]', col2) 
            print(array[0])
            # col3 = re.search("^[ 0-9]+$", col3)
            string.append({"title": title, "detail": col1, 'max_person': array[0], 'price': col3})
            if len(title) != 0:
                print("ok")
            else:
                print("else")
                get_table()
        print("string = ", string)
        # return string
    except Exception as exc:
        print(exc)
        print("catch")
        get_table()

def house_rules():
    checkInHtml = soup.find(id='checkin_policy')
    checkinTime = checkInHtml.find(class_='timebar__label').get_text()

    checkOutHtml = soup.find(id='checkout_policy')
    checkoutTime = checkOutHtml.find(class_='timebar__label').get_text()

    cancelHtml = soup.find(id='cancellation_policy')
    cancelTtitle = cancelHtml.select('span')[1].get_text()
    cancelContent = cancelHtml.select('p')[1].get_text()

    cancelTtitle = cancelTtitle.replace("\n", "")
    cancelContent = cancelContent.replace("\n", "")

    childContent = soup.find(class_='child-policies-table-wrapper').get_text()
    childContent = childContent.replace("\n", "")

    petHtml = soup.find(class_='description description--house-rule')
    petContent = petHtml.find_all('p')[1].get_text()

    string = []
    string.append({'checkin': checkinTime, 'checkout': checkoutTime, cancelTtitle : cancelContent, 'Child_policies':childContent,'pet':petContent})
    return string

def getlocation():
    text = soup.select('.hp_address_subtitle')[0].get_text()
    text = text.replace("\n", " ")
    return text  # string

def gettitle():
    title = soup.select('.hp__hotel-name')[0].get_text()
    title = title.replace("\n", " ")
    return title

def getImage():
  

    dict_list = []
    # print(soup)
    try:
        driver.get(URL)
        soup_image = BeautifulSoup(driver.page_source, 'lxml')


        for tag in soup_image.find_all(class_="hprt-roomtype-link"):

            print(tag.get('id'))
            driver.find_element_by_xpath('//*[@id="' + tag.get('id') + '"]/span').click()
            soup1 = BeautifulSoup(driver.page_source, 'lxml')


            link = soup1.find_all('div', { 'class' :"slick-track"})
            # print(str(link))
            soup2 = BeautifulSoup(str(link), 'html.parser')
            time.sleep(0.5)
            # title = driver.find_element_by_class_name('hprt-roomtype-icon-link').text
            # title = driver.find_element_by_xpath('//*[@id="hp_rt_room_gallery_modal_room_name"]')
            # print("title ",title)
            # time.sleep(0.5)
            img = soup2.select('img')
            print(len(img))
            # print("title image = ", title)
            for i in range(0, len(img)):
                src = img[i].get("src")
                lazy = img[i].get("data-lazy")
                if src:
                    dict_list.append({"img_link" + str(i +1): src})
                if lazy:
                    dict_list.append({"img_link" + str(i +1): lazy})

            driver.find_element_by_class_name('modal-mask-closeBtn').click()
            time.sleep(1)
        return dict_list
    except Exception as exc:
        print(exc)
        driver.close()
        getImage()
    # return dict_list

if __name__ == '__main__':
    # house_rules()
    print("###################")
    print("###################")
    
    link = getImage()
    get_table()
    print("table", string)
    json_raw = {
        'title': gettitle(),
        'cotent': get_content(),
        'table': string,
        'house_rule': house_rules(),
        'image_link': link,
        'location': getlocation()
    }
    json_string = json.dumps(json_raw)
    f = open("demo.json", "a")
    f.write(json_string)
    f.close()
    driver.close()